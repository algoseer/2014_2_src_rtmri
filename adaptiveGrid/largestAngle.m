function ang=largestAngle(pts,idx)
%Find the largest angular gap with the idx^th point as the center between the points pts

	N=size(pts,1);
	pt=pts(idx,:);
	pts(idx,:)=[];
	pts=pts-repmat(pt,N-1,1);

	%[theta,rho]=cart2pol(pts(:,1),pts(:,2));
	theta=atan2(pts(:,1),pts(:,2));

	theta_sorted=sort(theta);

	dtheta=diff(theta_sorted);
	dtheta=[dtheta;2*pi-theta_sorted(end)+theta_sorted(1)];
	ang=max(dtheta);

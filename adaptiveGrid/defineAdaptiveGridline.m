%% Author : Naveen Kumar
%% Created : Feb. 9, 2012
%% Code : Finds an airway per frame and define gridline on the image
%% Version : 2 . shared with jangwon

function defineAirwayPath(ind)

%files=dir('~/experiments/SPAN/mri/data/*mat');
%files(ind).name
%load(['~/experiments/SPAN/mri/data/' files(ind).name]);

files=dir('~/experiments/SPAN/mri/data/*avi');
files(ind).name
mri_data=readFullVideo(['~/experiments/SPAN/mri/data/' files(ind).name]);


%smooth the mri_image
avg_img=mean(mri_data);
avg_img=reshape(avg_img,68,68);
sens_map=medfilt2(avg_img,[10 10])>20;


mri_data=f_post_proc3(mri_data,0.9);

size(mri_data,1)

for t=1:size(mri_data,1)
		
	img=mri_data(t,:);
	img=double(img);
	img=reshape(img,68,68);

	img1=edge(img,'canny');
	[idx,idy]=find(img1);

%	figure(1)
%	subplot(111);imagesc(img);title('Image with detected edge points');
%	set(get(gca,'Title'),'FontSize',15);
%	hold on; plot(idy,idx,'w*');


	clr_img=zeros(size(img));
	ang=zeros(length(idx),1);
	for i=1:length(idx)
		ang(i)=largestAngle([idx idy],i)*180/pi;
		clr_img(idx(i),idy(i))=ang(i);
	end	

	%Find nosetip and chin
	th=195;
	idx=idx(ang>th);idy=idy(ang>th);

	cx=mean(idx);cy=mean(idy);
	th=atan2(idx-cx,idy-cy);
	[~,id]=sort(th);
	sort_idx=idx(id);sort_idy=idy(id);

	sort_idx(2:end-1)=[];	
	sort_idy(2:end-1)=[];	

%	figure(1);
%	subplot(111)
%	plot(sort_idy,sort_idx,'g^','MarkerSize',20);
%	plot(sort_idy,sort_idx,'wx-','LineWidth',3);hold off;

	nosetip=[sort_idx(1) sort_idy(1)];
	chin=[sort_idx(2) sort_idy(2)];


	%find the lips
	figure(1)
	subplot(231);
	imagesc(clr_img);hold on;
	plot([nosetip(2) chin(2)],[nosetip(1) chin(1)],'wo-');
	alip=ceil((nosetip(end,:)+chin(end,:))/2);
	plot(alip(2),alip(1),'g+','MarkerSize',8);	%Approximate lip
	hold off;
	title('Nosetip and chin detection')
	set(get(gca,'Title')','FontSize',12)



	%brightness mask to use later for flood fill
	mask= img>0 & img<max(img(:))/2;
	mask(:,52:end)=0;
	mask(1:nosetip(1),:)=0;
	mask(clr_img>50)=0;	


	%Edge image of the vocal tract
	tract=clr_img<50;
	tract(:,52:end)=0;
	tract(1:nosetip(1),:)=0;


	subplot(232);
	startpt=alip;
	endpt=[60,47];
	imagesc(img1);
	hold on;
	plot([startpt(2) endpt(2)],[startpt(1) endpt(1)],'rx','MarkerSize',10,'LineWidth',2);hold off;
	title('Vocal airway start-end');
	set(get(gca,'Title')','FontSize',12)


	subplot(233);
	L=bwlabel(tract&img1);
	%Count the number of pieces in each segment
	[~,count]=unique(sort(L(:)));
	count=diff(count);
	num_comp=max(L(:));
	for kk=1:num_comp
		L(L==kk)=count(kk);
	end
	tract=L>20;
	

	%save L.mat L

	imagesc(tract);
	title({'Segmented vocal tract';'using morphological constraints'});
	set(get(gca,'Title')','FontSize',12)

	marker=tract;


	%Create the mask using negative of the original intensity image and sensitity map
	mask(~sens_map)=0;
	marker=marker&mask;

	img_rec=imreconstruct(marker,mask,4);
	img_rec_skel=bwmorph(img_rec,'skel','Inf');

	subplot(234);
	imagesc(img_rec);
	title('Morphological reconstruction or filling');
	set(get(gca,'Title')','FontSize',12)

	subplot(235);
	imagesc(img_rec_skel);
	title('Skeletonization');
	set(get(gca,'Title')','FontSize',12)


	%save vocal_tract.mat img_rec_skel;

	[idx,idy]=find(img_rec_skel);
	[airpath,~]=shortestAirCavity([idx idy],startpt,endpt,10);



	if length(airpath)>0
		airpath=interparc(20,airpath(:,1),airpath(:,2),'linear');
		%Smooth the points
		airpath=medfilt1(airpath,5);

		%Find the normal directions and show the gridline
		d=diff(airpath);
		d=medfilt1(d,5);
		u=d(:,1);v=-d(:,2);

		subplot(236);
		imagesc(img);hold on;colormap(hot)
		plot(airpath(:,2),airpath(:,1),'x-','LineWidth',2);
		quiver(airpath(2:end,2),airpath(2:end,1),u,v,1);
		quiver(airpath(2:end,2),airpath(2:end,1),-u,-v,1);
		hold off;
		title('Gridline defined on the shortest path');
		set(get(gca,'Title')','FontSize',12)




		%Find the center of the darkest region along the line perpendicular
%		cc=10;
%		d=sqrt(u.^2+v.^2);
%		u=u./d;v=v./d;
%		
%		stpts=airpath(2:end,:)+cc*[v u];
%		enpts=airpath(2:end,:)-cc*[v u];
%		allpts=[stpts enpts];
%		%allpts=reshape(allpts',2,2*size(stpts,1))';
%
%		subplot(237);
%		imagesc(img_rec);hold on;
%		plot(stpts(:,2),stpts(:,1),'g.-');
%		plot(enpts(:,2),enpts(:,1),'m.-');
%		for tt=1:length(allpts)
%			%plot(allpts(:,2),allpts(:,1),'r');
%			plot([allpts(tt,2) allpts(tt,4)],[allpts(tt,1) allpts(tt,3)],'r');
%		end
%		plot(airpath(:,2),airpath(:,1),'.-');hold off
%		axis ij
%
%
%		subplot(3,3,8:9);
%		for i=1:length(airpath)-1
%			improfile(img,stpts(i,[2 1]),enpts(i,[2 1]));
%			pause(0.1);
%		end

	end
	pause(0.1)
end

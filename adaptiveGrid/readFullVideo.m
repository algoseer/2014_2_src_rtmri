function imgMat=readFullVideo(filename)

v=VideoReader(filename);
imgMat=zeros(v.NumberOfFrame,v.Height*v.Width);


for t=1:v.NumberOfFrames
	im=flipud(rgb2gray(v.read(t)));
	%im=rgb2gray(v.read(t));
	imgMat(t,:)=reshape(im,1,v.Height*v.Width);
end

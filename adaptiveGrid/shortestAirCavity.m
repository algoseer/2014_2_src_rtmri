function [airpath,ind]=shortestAirCavity(pts,startpt,endpt,K)

%K=5;	%Number of nearest neighbours
%Create an adjacency matrix using only K nearest neighbours for each point

pts=[startpt;pts;endpt];
N=size(pts,1);

G=sparse(N,N);

[idx,d]=knnsearch(pts,pts,'K',K);
for i=1:N
	G(i,idx(i,:))=d(i,:);
end

%Construct the graph based on radius
%dth=10;
%d=pdist2(pts,pts);
%for i=1:N
%	idx=d(i,:)<dth;
%	G(i,idx)=d(i,idx);
%end


[~,path,~]=graphshortestpath(G,1,N);
ind=path(2:end-1)-1;
airpath=pts(path,:);
if length(path)==0
	disp('No path found')
end

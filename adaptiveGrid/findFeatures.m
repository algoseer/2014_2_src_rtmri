function findFeatures(ind)
files=dir('~/experiments/SPAN/mri/data/*mat');
%files=dir('~/experiments/SPAN/mri/data/*avi');
files(ind).name
load(['~/experiments/SPAN/mri/data/' files(ind).name]);
%$avi=aviread(['~/experiments/SPAN/mri/data/' files(ind).name]);
S=[];

%for t=1:length(avi);
for t=1:size(mri_data,1)
	%img=rgb2gray(avi(t).cdata);
	img=mri_data(t,:);
	img=double(img);
	img=reshape(img,68,68);

	img1=edge(img,'canny');
	[idx,idy]=find(img1);

%	figure(1)
%	subplot(111);imagesc(img);title('Image with detected edge points');
%	set(get(gca,'Title'),'FontSize',15);
%	hold on; plot(idy,idx,'w*');


	clr_img=zeros(size(img));
	ang=zeros(length(idx),1);
	for i=1:length(idx)
		ang(i)=largestAngle([idx idy],i)*180/pi;
		clr_img(idx(i),idy(i))=ang(i);
	end	
	%cv=convhull(idx,idy);

	figure(1)
	subplot(131);
	imagesc(img);hold on; plot(idy,idx,'.');hold off;
	colormap(hot);
	title('Edge detection');

	%Find nosetip and chin
	th=195;
	idx=idx(ang>th);idy=idy(ang>th);

	cx=mean(idx);cy=mean(idy);
	th=atan2(idx-cx,idy-cy);
	[~,id]=sort(th);
	sort_idx=idx(id);sort_idy=idy(id);

	sort_idx(2:end-1)=[];	
	sort_idy(2:end-1)=[];	

%	figure(1);
%	subplot(111)
%	plot(sort_idy,sort_idx,'g^','MarkerSize',20);
%	plot(sort_idy,sort_idx,'wx-','LineWidth',3);hold off;

	nosetip=[sort_idx(1) sort_idy(1)];
	chin=[sort_idx(2) sort_idy(2)];


	%find the lips
	subplot(132);
	imagesc(clr_img);hold on;
	plot([nosetip(2) chin(2)],[nosetip(1) chin(1)],'wo-');
	alip=ceil((nosetip(end,:)+chin(end,:))/2);
	plot(alip(2),alip(1),'g+','MarkerSize',8);
	hold off;
	title('Nosetip and chin detection')

	[idx,idy]=find(img1);
	contour=[idx(ang>120) idy(ang>120)];
	id=knnsearch(contour,alip,'K',10);
	lips=contour(id,:);

	hold on;
	plot(lips(:,2),lips(:,1),'w.')
	hold off;

	ed=unique(lips(:,1));
	[~,ind]=max(diff(ed));

	ulip=[ed(ind) min(lips(lips(:,1)==ed(ind),2))];
	llip=[ed(ind+1) min(lips(lips(:,1)==ed(ind+1),2))];

	subplot(133);
	imagesc(clr_img);
	hold on;
	if isStraightLine(unique(lips(:,1)))==1
		plot(lips(1,2),lips(1,1),'gx','LineWidth',2,'MarkerSize',10);
		plot(lips(2,2),lips(2,1),'gx','LineWidth',2,'MarkerSize',10);
		xlabel('Lips Closed');
	else
		plot(ulip(2),ulip(1),'g^','LineWidth',2,'MarkerSize',5);
		plot(llip(2),llip(1),'gv','LineWidth',2,'MarkerSize',5);
		xlabel('Lips Open');
	end
	hold off;
	title('Upper Lip and Lower Lip Detection')


	pause(0.3)

end

function y=isStraightLine(pts)

if length(unique(diff(pts)))==1
	y=1;
else
	y=0;
end

%function findNose(ind)
%files=dir('~/experiments/SPAN/mri/data/*mat');
%load(['~/experiments/SPAN/mri/data/' files(ind).name]);
nosetip=zeros(0,2);
chin=zeros(0,2);
for t=1:length(avi)%size(mri_data,1)
	img=avi(t).cdata;
	%img=mri_data(t,:);
	%img=double(img);
	%img=reshape(img,68,68);

	img1=edge(img,'canny');
	[idx,idy]=find(img1);

	figure(1)
%	subplot(211);imagesc(img);title('Image with detected edge points');
%	set(get(gca,'Title'),'FontSize',15);
%	hold on; plot(idy,idx,'wo');hold off;


	clr_img=zeros(size(img));
	ang=zeros(length(idx),1);
	for i=1:length(idx)
		ang(i)=largestAngle([idx idy],i)*180/pi;
		clr_img(idx(i),idy(i))=ang(i);
	end	
	%cv=convhull(idx,idy);


	subplot(121);
	imagesc(clr_img);title('Nosetip and Chin Detection');
	set(get(gca,'Title'),'FontSize',15);
	colormap(hot);

	th=190;

	idx=idx(ang>th);idy=idy(ang>th);

	cx=mean(idx);cy=mean(idy);
	th=atan2(idx-cx,idy-cy);
	[~,id]=sort(th);
	sort_idx=idx(id);sort_idy=idy(id);

%	figure(2);
%	imagesc(clr_img);
%	colormap(hot);
%	hold on;
%	plot(cy,cx,'r*','MarkerSize',20);
%	for k=1:length(id)
%		plot([sort_idy(k);cy],[sort_idx(k);cx],'g-','MarkerSize',15);
%		text(sort_idy(k)-2,sort_idx(k)+2,num2str(k),'color','w');
%	end
%	hold off;

	sort_idx(2:end-1)=[];	
	sort_idy(2:end-1)=[];	

	figure(1);
	subplot(121)
	hold on;
	plot(sort_idy,sort_idx,'g^','MarkerSize',20);
	plot(sort_idy,sort_idx,'wx-','LineWidth',3);hold off;

	nosetip=[nosetip;[sort_idx(1) sort_idy(1)]];
	chin=[chin;[sort_idx(2) sort_idy(2)]];

	%Estimate the transform
	subplot(122);
	hold on;
	plot([nosetip(end,2) chin(end,2)],[nosetip(end,1) chin(end,1)],'rx-');
	plot(nosetip(end,2),nosetip(end,1),'b^');
	plot(chin(end,2),chin(end,1),'kv');
	axis ij;
	axis([0 64 0 64])
	title('Tracking');
	set(get(gca,'Title'),'FontSize',15);

	if t>1
		d=nosetip(1,:)-nosetip(end,:);
		dbefore=chin(1,:)-nosetip(1,:);
		dnow=chin(end,:)-nosetip(end,:);
		dang=acos(dbefore*dnow'/(norm(dbefore)*norm(dnow)))*180/pi;

		figure(2)
		subplot(131);
		imagesc(avi(1).cdata);
		xlabel('(a)');set(get(gca,'XLabel'),'FontSize',20);
		subplot(132);
		imagesc(img);
		xlabel('(b)');set(get(gca,'XLabel'),'FontSize',20);
		subplot(133);
		trf = maketform('affine', [1 0 0; 0 1 0; d(2) d(1) 1]);
		timg=imtransform(img,trf,'XData',[1 size(img1,2)], 'YData',[1 size(img1,1)]);
		imagesc(rotateAround(timg,nosetip(1,2),nosetip(1,1),dang));
		xlabel('(c)');set(get(gca,'XLabel'),'FontSize',20);
		colormap(hot);
		dang

	end


	pause(1)

end

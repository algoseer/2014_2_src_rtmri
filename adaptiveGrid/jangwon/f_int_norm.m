function output = f_int_norm(input,int_min,int_max)

if     nargin == 1
    int_min = 0;
    int_max = 255;
elseif nargin == 2
    int_max = 255;
end

% outlier rejection
input2= reshape(input,1,[]);
q90 = quantile(input2,0.9); 
q90_idx = find(input2 > q90);
q10 = quantile(input2,0.1); 
q10_idx = find(input2 < q10);
input2(q90_idx) = q90;
input2(q10_idx) = q10;
input3 = reshape(input2,68,68);

% substract the min intensity
input4 = input3 - min(min(input3));

output = ((input4 ./ max(max(input4))) * (int_max - int_min)) + int_min;
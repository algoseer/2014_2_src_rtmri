function img3 = f_sigmoid_kernel_highlighting(img_nr2,w)

% dum = reshape(img_nr2,1,68*68);
[N,X]=hist(reshape(img_nr2,1,68*68),50);
%     bar(X,N); pause;
[val,idx]=sort(N,'descend');
sig_thresh = X(idx(2));
sig_thresh = sig_thresh / 2;
img3 = w*(img_nr2-sig_thresh);
img3 = 1./(exp(-double(img3))+1);
img3 = f_int_norm(img3,1,255);
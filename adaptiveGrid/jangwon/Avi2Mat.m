function M = Avi2MovieMat_jw(filename)
%
% Jangwon Kim (2013)
%
% Convert avi file to matrix of frames by using mmreader
%
% INPUT: file path
% OUTPUT: the movie matrix (row: frame, col: pixel index)

xyloObj = mmreader(filename);

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

mov(1:nFrames) = ...
    struct('cdata', zeros(vidHeight, vidWidth, 1, 'uint8'),...
           'colormap', []);
       
% Read one frame at a time.
for k = 1 : nFrames
    mov(k).cdata = read(xyloObj, k);
end

vec_length = vidHeight*vidWidth;

M = zeros(nFrames,vec_length);
for itor = 1:nFrames
    M(itor,:) = reshape(double(mov(itor).cdata(:,:,1)),1,vec_length);
end


%eof

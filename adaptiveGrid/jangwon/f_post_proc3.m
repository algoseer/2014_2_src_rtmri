function output = f_post_proc3(data_mri,PCA_thld)

% input: 
%   data_mri: matrix of rtMRi video (row: frame; col: pixel)
%   PCA_thld: PCA threshold [0-1]. Usually set to be 0.9 not to lose fastly
%     varying movement in the image
% output: matrix of pre-processed rtMRi video (row: frame; col: pixel)

k=5; % median filter
w=0.05; % stiffness of curve (higher w -> stiffer)

H = fspecial('average', [7 7]);

% noise reduction
%data_mri_nr = f_post_proc_nrOnly(data_mri, PCA_thld);
% wiener_param = [2 2];
% data_mri_nr = f_wiener_noise_reduction(data_mri,wiener_param);
data_mri_nr=data_mri;


output = zeros(size(data_mri));
for t=1:size(data_mri,1)
% for t=30
%     t

    % orig image
    img=reshape(data_mri_nr(t,:),68,68);
    img_nr = f_int_norm(img,0,255);
    
    % image smoothing
    outimg=medfilt2(imclose(img,strel('disk',10)),[k k]);
    outimg_sm=outimg;
    img_edge=edge(img_nr,'canny'); % canny gives one connected edge
    img_edge = zeros(size(img_edge));
    sm_img_edge=edge(outimg,'canny'); % canny gives one connected edge
    
    

    % find the left-most bnd
    img_lm_bnd = zeros(size(img_edge));
    for i=1:68
        bnd_idx1 = find(img_edge(i,:) > 0);  % col1: y (vertical), col2: x (horizontal)
        bnd_idx2 = find(sm_img_edge(i,:) > 0);  % col1: y (vertical), col2: x (horizontal)
        if     (isempty(bnd_idx1) == 0) && (isempty(bnd_idx2) == 0)
            min_x = max([1 min(bnd_idx1) min(bnd_idx2)]);
        elseif (isempty(bnd_idx1) == 0) && (isempty(bnd_idx2) == 1)
            min_x = max([1 min(bnd_idx1)]);
        elseif (isempty(bnd_idx1) == 1) && (isempty(bnd_idx2) == 0)
            min_x = max([1 min(bnd_idx2)]);
        else
            min_x = 69;
        end
        max_x = 68;
        min_y = max([(i-1) 1]);
        max_y = min([(i+1) 68]);
        img_lm_bnd(min_y:max_y,min_x:max_x) = 1;
    end
    
    LM_x = zeros(68,1);
    for i=1:68
        bnd_idx = find(img_lm_bnd(i,:) > 0);  % col1: y (vertical), col2: x (horizontal)
        if (isempty(bnd_idx) == 0)
            min_x = max([1 min(bnd_idx)]);
        else
            min_x = 68;
        end
        LM_x(i) = min_x;
    end
%     keyboard;
    % interpolate
    x=1:68;
    LM_x_sm = round(smooth(x,LM_x,0.1,'rloess'));

    % LM edge -> right-surface
    LM_bnd = zeros(size(img,1));
    for i=1:68
        LM_bnd(i,max([1 min([LM_x_sm(i) 68])]):end) = 1;
    end
    
    % 
    img_nr_sm = LM_bnd.*img_nr;
    
    outimg = imfilter(outimg, H);
    outimg = f_int_norm(outimg,1,255);
    outimg(~LM_bnd)=1;
    idx = find(outimg < 1); outimg(idx) = 1;
    
    % intensity correction
    img_nr_sm = f_int_norm(img_nr_sm,0,255);
%     keyboard;
    img_nr2=(img_nr_sm)./(outimg);
    img_nr2 = f_int_norm(img_nr2,0,255);

    % sigmoid function based contrasting
    img3 = f_sigmoid_kernel_highlighting(img_nr2,w);
%     img4 = f_sigmoid_kernel_highlighting(img3,w);
%     img5 = f_sigmoid_kernel_highlighting(img4,w);
    
    % final smoothing
%     img3_sm = wiener2(img3,[2 2]);
%     img3_sm2 = medfilt2(img3,[3 3]);
%     img3_sm = medfilt2(img3,[2 2]);
    
    output(t,:) = reshape(img3,1,68*68);
    
%     keyboard;
   
%     % plot
%     subplot(341);imagesc((img));title('orig image'); colormap(gray);
%     subplot(342);imagesc((img_edge));title('edge image'); colormap(gray); pause(0.01);
%     subplot(343);imagesc((outimg_sm));title('sm orig image'); colormap(gray);
%     subplot(344);imagesc((sm_img_edge));title('edge sm image'); colormap(gray);
%     subplot(345);imagesc((LM_bnd));title('LM edge sm image'); colormap(gray);
%     subplot(346);imagesc(img_nr_sm);title('Sel image'); colormap(gray);
%     subplot(347);imagesc(outimg);title('Int map'); colormap(gray); 
%     subplot(348);imagesc(img_nr2);title('Int Cor image'); colormap(gray); 
%     subplot(349);imagesc(img3);title('Cont image (SIG)'); colormap(gray);
% %     tmp_w=1; img4 = f_sigmoid_kernel_highlighting(img_nr2,tmp_w);
% %     subplot(3,4,10);imagesc(img4);title('Cont image (SIG); w=1'); colormap(gray);
% %     tmp_w=50; img5 = f_sigmoid_kernel_highlighting(img_nr2,tmp_w);
% %     subplot(3,4,11);imagesc(img5);title('Cont image (SIG); w=50'); colormap(gray);
% %     subplot(3,4,12);imagesc(img3_sm);title('sm Cont image (SIG)'); colormap(gray);
%     
% %     subplot(337);imagesc(img3);title('Int map'); colormap(gray); 
% %     subplot(338);imagesc(img4);title('Int Cor image'); colormap(gray); 
% %     subplot(339);imagesc(img5);title('Cont image (SIG)'); colormap(gray);
%     
%     pause;
%     
% %     subplot(336);imagesc(img3_sm3);title('Sel image'); colormap(gray);
% %     subplot(337);imagesc(img3_sm2);title('Int map'); colormap(gray); 
% %     subplot(338);imagesc(img3);title('Int Cor image'); colormap(gray); 
% %     subplot(339);imagesc(img3_sm);title('Cont image (SIG)'); colormap(gray);
% 
% %     pause(0.01);

    
end

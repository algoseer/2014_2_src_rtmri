function output = f_post_proc_nrOnly(data_mri, PCA_thld)

% addpath ./pca_noise_reduction_by_adam

M=data_mri;

% h=figure;
% map_dims = 64;
% % for frame_itor = 1:size(M1,1)
% for frame_itor = 1:200
%     subplot(1,2,1);
%     image(reshape(M1(frame_itor,:).*(map_dims/max(max(M1))),68,68));
%     title('Raw Image');
%     subplot(1,2,2);
%     image(reshape(M(frame_itor,:).*(map_dims/max(max(M))),68,68));
%     title('Intensity correction');
%     pause(0.02);
% end
% close(h);
%
% PCA noise reduction
MN = mean(M,1)'; %mean
%Principal Component Analysis (PCA)
% keyboard;
[coeffs, scores, latent] = princomp(M);
latent = latent./sum(latent);

%Determine the Amount of Variance to Keep
cutoff = min(find(cumsum(latent)>PCA_thld));

%PCA Reconstruction Filter
N = zeros(size(M));
for frame_itor = 1:size(M,1) %for each frame
    built = zeros(size(M,2),1);
    
    %build the reconstructed frame
    % (add weighted components, iteratively)
    for comp_itor = 1:cutoff                     %for each component
        comp = coeffs(:,comp_itor);              %get the coefficients 
        contrib = scores(frame_itor,comp_itor);  %get the component weight
        built = built + contrib.*comp;           %add it in!
    end
    
    %add the reconstruction to the mean
    built = MN+built;
    
    %store the frame
    N(frame_itor,:) = built';
end

% %%
% h=figure;
% map_dims = 68;
% for frame_itor = 1:size(M1,1)
%     subplot(1,2,1);
%     image(reshape(M1(frame_itor,:).*(map_dims/max(max(M1))),68,68));
%     title('Raw Image');
%     subplot(1,2,2);
%     image(reshape(N(frame_itor,:).*(map_dims/max(max(N))),68,68));
%     title('PCA Filt');
%     pause(0.02);
% end
% close(h);
output = N;
%% Author : Naveen Kumar
%% Created : Mar 27, 2012
%% Edited : Mar 28, 2012
%% Code : Annotates outer and inner vocal tract given a rtMRI video 
%% Features : Two reset modes :
%% Up reset : Reset previous frames annotation ; Left reset : Reset this frame's annotation

function locs= annotateVideos(ind)
fileType='mat';
annoteFolder='../videos/lac09302012/kf1_lab/';
vidFolder='../videos/lac09302012/kf1/';
files=dir([vidFolder '*' fileType]);
files(ind).name 

load([vidFolder files(ind).name]);
%mri_data=readAVIvideo(['../videos/' files(ind).name]);

size(mri_data,1)

locs={};

matfilename=files(ind).name(1:end-4);

%Check if an annotation already exists for this file and load upto that point
load([annoteFolder matfilename '.mat']);


for t=1:size(mri_data,1)
		
	img=mri_data(t,:);
	img=double(img);
	img=reshape(img,68,68);

	imagesc(img);colormap(hot);

	hold on;
	plot(locs{t}.lower_VT(:,1),locs{t}.lower_VT(:,2),'x-');
	plot(locs{t}.upper_VT(:,1),locs{t}.upper_VT(:,2),'x-');
	hold off;

	pause(0.5)	
	t=t+1;
end

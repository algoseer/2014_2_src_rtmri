%% Author : Naveen Kumar
%% Created : Mar 27, 2012
%% Edited : Mar 28, 2012
%% Code : Annotates outer and inner vocal tract given a rtMRI video 
%% Features : Two reset modes :
%% Up reset : Reset previous frames annotation ; Left reset : Reset this frame's annotation

function locs= annotateVideos(ind)
close all
fileType='mat';
annoteFolder='../annotated/';
vidFolder='../videos/';
files=dir([vidFolder '*' fileType]);
files(ind).name 

load([vidFolder files(ind).name])
%mri_data=readAVIvideo(['../videos/' files(ind).name]);

size(mri_data,1)

locs={};

matfilename=files(ind).name(1:end-4);

%Check if an annotation already exists for this file and load upto that point
if exist([annoteFolder matfilename '.mat'])
	load([annoteFolder matfilename '.mat']);
	t=length(locs)+1;
else
	locs={};
	t=1;
end


while t<=size(mri_data,1)
		
	img=mri_data(t,:);
	img=double(img);
	img=reshape(img,68,68);


	
   	redo=1;
    	while redo>0
        
        	imagesc(img);colormap(gray);
       		if redo>1
       		        title('Reset')
                	pause(0.5);
        	end
        	title([num2str(t) ':Mark the outer vocal tract']);
        	set(get(gca,'Title'),'FontSize',15);
        	upper_VT=imfreehand('Closed',false);
        	loc_upper_VT=getPosition(upper_VT);
        
		%Reset annotation to this frame
        	if any(loc_upper_VT(:,1)<0)   
           	 	redo=redo+1;
            		continue;
        	end
		%Reset to previous frame
		if any(loc_upper_VT(:,2)<0)   
           	 	redo=redo+1;
			t=t-1;
			img=mri_data(t,:);
			img=double(img);
			img=reshape(img,68,68);
            		continue;
        	end

        	title([num2str(t) ':Mark the inner vocal tract']);
        	set(get(gca,'Title'),'FontSize',15);
        	lower_VT=imfreehand('Closed',false);
        	loc_lower_VT=getPosition(lower_VT);
        
		%Reset annotation to this frame
        	if any(loc_lower_VT(:,1)<0)
           	 	redo=redo+1;
            		continue;
        	end
		%Reset to previous frame
		if any(loc_lower_VT(:,2)<0)   
           	 	redo=redo+1;
			if t>1
				t=t-1;
			end
			img=mri_data(t,:);
			img=double(img);
			img=reshape(img,68,68);
            		continue;
        	end

                
        locs{t}.upper_VT=loc_upper_VT;
    	locs{t}.lower_VT=loc_lower_VT;
        
	%Save the mat file with annotation upto this point
	save([annoteFolder matfilename '.mat'],'locs');
        redo=0;
    end
    t=t+1;
end
